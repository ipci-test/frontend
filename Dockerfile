FROM node AS builder

ARG api_url=/api

WORKDIR /app

COPY ./vue-realworld-example-app-master .

# Set the api URL - doing this here, at build time, is not ideal but sufficient given that a proxy translates /api to the backend service
RUN sed -i "s|https://conduit.productionready.io/api|$api_url|" src/common/config.js

RUN yarn install && yarn build && rm -rf node_modules

FROM nginx
COPY --from=builder /app/dist /usr/share/nginx/html
