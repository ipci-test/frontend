# Realworld frontend application

This is a [RealWorld](https://github.com/gothinkster/realworld-example-apps) frontend application using a [copy](./vue-realworld-example-app-master) of the [Vue example implementation](https://github.com/gothinkster/vue-realworld-example-app).

## Usage

The following shows how to build and run it using a [Dockerfile](./Dockerfile) (to run the application without docker, please see the [source docs](./vue-realworld-example-app-master/README.md)).

```bash
# Build the image:
docker build \
  # Use an existing backend - by default that URL is "/api"
  --build_arg api_url="https://conduit.productionready.io/api" \
  -t myfrontend .

# Run it:
docker run -p 8080:8080 myfrontend

# Check that it works:
curl http://localhost:8080/api/articles # Should output {"articles":[],"articlesCount":0}
```

## Configuration

The only configuration option is the api_url which for now can only be changed on build time using the build arg `api_url` which by default is `"/api"`. It must point to the API endpoint of a [RealWorld backend implementation](https://github.com/gothinkster/realworld#backends).
